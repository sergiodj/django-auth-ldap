django-auth-ldap (4.1.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.6.1.0.
  * Update year in d/copyright.
  * Clean up django_auth_ldap/version.py to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Wed, 06 Jul 2022 18:58:33 +0000

django-auth-ldap (4.0.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.6.0.1.
  * Update project homepage URL.
  * Disable build-time tests as they require a live slapd instance.
  * Build using pybuild-plugin-pyproject and python3-setuptools-scm.
  * Enable upstream testsuite for autopkgtests.

 -- Michael Fladischer <fladi@debian.org>  Sun, 19 Jun 2022 08:11:28 +0000

django-auth-ldap (3.0.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump Standards-Version to 4.5.1.
  * Use uscan version 4.
  * Clean up egg-info files to allow two builds in a row.

 -- Michael Fladischer <fladi@debian.org>  Sun, 15 Aug 2021 14:22:38 +0000

django-auth-ldap (2.2.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Bump debhelper version to 13.

 -- Michael Fladischer <fladi@debian.org>  Sat, 06 Jun 2020 23:15:59 +0200

django-auth-ldap (2.1.1-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Bump Standards-Version to 4.5.0.
  * Remove debian/upstream-signing-key.pgp from include-binaries, no
    longer used.
  * Remove unused Files-Excluded from debian/copyright.

 -- Michael Fladischer <fladi@debian.org>  Sun, 26 Apr 2020 21:48:35 +0200

django-auth-ldap (2.1.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Set Rules-Requires-Root: no.

 -- Michael Fladischer <fladi@debian.org>  Wed, 11 Dec 2019 09:21:17 +0100

django-auth-ldap (2.0.0-2) unstable; urgency=medium

  * Team upload.
  * Reupload without binaries to allow migration.

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 07 Aug 2019 12:36:55 +0500

django-auth-ldap (2.0.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches and drop Python2 compatibility patch.
  * Remove Python2 support.
  * Bump debhelper compatibility and version to 12 and switch to
    debhelper-compat.
  * Bump Standards-Version to 4.4.0.
  * Remove signing key as upstream releases are no longer signed.

 -- Michael Fladischer <fladi@debian.org>  Wed, 17 Jul 2019 10:29:36 +0200

django-auth-ldap (1.7.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Michael Fladischer ]
  * New upstream release.
  * Disable PGP signature check in uscan as upstream is not providing a
    signature for current releases.
  * Refresh patches.
  * Add patch to open tests.ldif as UTF-8 for Python 2 compatibility.
  * Add ldap-utils and slapd to Build-Depends as tests depend on them.
  * Change source directory for documentation.
  * Add python(3)-mock to Build-Depends to satisfy test requirements.
  * Bump Standards-Version to 4.2.1.

 -- Michael Fladischer <fladi@debian.org>  Sat, 22 Dec 2018 21:04:48 +0100

django-auth-ldap (1.4.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Michael Fladischer ]
  * New upstream release.
  * Bump debhelper compatibility and version to 11.
  * Refresh patches and drop patch for using pyldap in Python2.
  * Bump Standards-Version to 4.1.3.
  * Build documentation in override_dh_sphinxdoc.

 -- Michael Fladischer <fladi@debian.org>  Thu, 29 Mar 2018 09:30:06 +0200

django-auth-ldap (1.3.0-1) unstable; urgency=low

  * New upstream release (Closes: #881332).
  * Move upstream signing key to debian/upstream/signing-key.asc.
  * Refresh patches.
  * Update test runner to work with upstream changes (no more
    manage.py).
  * Bump Standards-Version to 4.1.1.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Enable autopkgtest-pkg-python testsuite.
  * Use https:// for uscan URL.

 -- Michael Fladischer <fladi@debian.org>  Fri, 24 Nov 2017 12:17:08 +0100

django-auth-ldap (1.2.15-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for 1.2.15.

 -- Michael Fladischer <fladi@debian.org>  Sun, 20 Aug 2017 14:55:19 +0200

django-auth-ldap (1.2.14+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches after git-dpm to gbp pq conversion
  * Refresh patches for 1.2.14.

 -- Michael Fladischer <fladi@debian.org>  Sat, 29 Jul 2017 23:27:29 +0200

django-auth-ldap (1.2.13+dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Thu, 06 Jul 2017 21:04:57 +0200

django-auth-ldap (1.2.12+dfsg-1) unstable; urgency=low

  * Upload to unstable.
  * Bump Standards-Version to 4.0.0.

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Jun 2017 20:40:10 +0200

django-auth-ldap (1.2.12+dfsg-1~exp1) experimental; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Jun 2017 20:39:14 +0200

django-auth-ldap (1.2.11+dfsg-1~exp1) experimental; urgency=low

  * New upstream release.

 -- Michael Fladischer <fladi@debian.org>  Fri, 12 May 2017 15:30:55 +0200

django-auth-ldap (1.2.10+dfsg-1~exp1) experimental; urgency=low

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

  [ Michael Fladischer ]
  * New upstream release.
  * Require mockldap-0.2.7 or later for Build-Depends.
  * Bump Standards-Version to 3.9.8.
  * Use https:// for copyright-format 1.0 URL.
  * Set minimum Python2 version to 2.6.
  * Use python-pyldap instead of python-ldap in Build-Depends.
  * Remove version constraint from python-sphinx in Build-Depends, no
    longer necessary.
  * Remove version constraint from python-django and python-django-doc
    in Build-Depends, no longer necessary.
  * Remove python-ldap from manually set Depends, python-pyldap is
    picked up automatically instead.
  * Remove version constraint from python-django in Depends, no longer
    necessary.
  * Add Python3 support through a separate binary package.
  * Simplify autho-tests so they also test Python3.
  * Switch to python3-sphinx.

 -- Michael Fladischer <fladi@debian.org>  Sun, 19 Mar 2017 09:02:57 +0100

django-auth-ldap (1.2.7+dfsg-1) unstable; urgency=medium

  [ Michael Fladischer ]
  * New upstream release (Closes: #806344).
  * Bump debhelper compatibility and version to 9.
  * Clean django_auth_ldap.egg-info/requires.txt to allow two builds in a row.

  [ SVN-Git Migration ]
  * git-dpm config
  * Update Vcs fields for git migration

 -- Michael Fladischer <fladi@debian.org>  Sat, 02 Jan 2016 16:22:15 +0100

django-auth-ldap (1.2.6+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.6.
  * Use pypi.debian.net service for uscan.
  * Add repacksuffix to d/watch for DFSG-compliant repacks.
  * Add dversionmangle for DFSG-repack to d/watch.
  * Change my email address to fladi@debian.org.

 -- Michael Fladischer <fladi@debian.org>  Thu, 28 May 2015 09:59:30 +0200

django-auth-ldap (1.2.2+dfsg-1) unstable; urgency=low

  * New upstream relese (Closes: #755588).
  * Add dh-python to Build-Depends.
  * Switch buildsystem to pybuild.
  * Remove superfluous install file.
  * Use upstream test environment.
  * Rename license to BSD-2-clause.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Tue, 23 Sep 2014 21:15:08 +0200

django-auth-ldap (1.2.0+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Remove minified JavaScript files from source tarball.
  * Mangle "+dfsg" in debian/watch.
  * Check PGP signature on upstream tarball:
    + Add signature URL to debian/watch.
    + Include upstream public PGP key D0FA0E76.
    + Allow debian/upstream-signing-key.pgp to be included as a binary.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Wed, 23 Apr 2014 12:54:38 +0200

django-auth-ldap (1.1.8-1) unstable; urgency=medium

  * New upstream release.
  * Update years in d/copyright.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Wed, 12 Feb 2014 09:55:01 +0100

django-auth-ldap (1.1.7-2) unstable; urgency=medium

  * Add mockldap to Build-Depends, reenabling tests.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Fri, 27 Dec 2013 14:39:48 +0100

django-auth-ldap (1.1.7-1) unstable; urgency=low

  [ Michael Fladischer ]
  * New upstream release (Closes: #678703).
  * Add python-setuptools to Build-Depends, upstream switched from distutils
    to setuptools.
  * Bump python version support to >= 2.5.
  * Bump dependency on Django to >= 1.3.
  * Bump Standards-Version to 3.9.5.
  * Bump debhelper in Build-Depends to >= 8.1.0~.
  * Drop unnecessary versioned dependencies in Depends and Build-Depends:
    + python-all
    + python-ldap
  * Ommit installing archived documentation:
    + Add patch docs-do_not_install_archive.patch.
  * Use locally installed objects.inv for intersphinx mapping:
    + Add patch docs-use_local_intersphinx_mapping.patch.
    + Add python-doc to Build-Depends.
    + Add python-django-doc to Build-Depends.
  * Format packaging files with wrap-and-sort.
  * Change documentation source directory for sphinx.
  * Update years in d/copyright.
  * Drop temporary fix for PKG-INFO.
  * Run dh_clean in override target.
  * Clean django_auth_ldap.egg-info/SOURCES.txt after each build.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Tue, 26 Nov 2013 08:57:20 +0100

django-auth-ldap (1.0.19-2) unstable; urgency=low

  * Make dependency on python-django unversioned again.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Sun, 08 Jul 2012 20:03:30 +0200

django-auth-ldap (1.0.19-1) unstable; urgency=low

  * New upstream release.
  * Fix tests for django (>= 1.4) FTBFS (Closes: #669486).
  * Bump dependency on python-django to (>= 1.2).

 -- Michael Fladischer <FladischerMichael@fladi.at>  Sun, 08 Jul 2012 14:07:46 +0200

django-auth-ldap (1.0.18-1) unstable; urgency=low

  * New upstream release.
  * Update years in d/copyright.
  * Update DEP-5 URL to 1.0.
  * Bump Standards-Version to 3.9.3.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Fri, 16 Mar 2012 09:28:59 +0100

django-auth-ldap (1.0.14-1) unstable; urgency=low

  * New upstream release.
  * B-D on python-sphinx (>= 1.0.7+dfsg).
  * Change order of my name.
  * Temporary fix for Metadata-Version in django_auth_ldap-1.0.14.egg-
    info.

 -- Michael Fladischer <FladischerMichael@fladi.at>  Mon, 09 Jan 2012 12:31:28 +0100

django-auth-ldap (1.0.12-1) unstable; urgency=low

  * New upstream release.
  * Upload to unstable.
  * Update DEP5 format.
  * Drop debian/pyversions in favour of XS-Python-Version.
  * Set PMPT as maintainer and myself as uploader.
  * Simplify debian/rules by using static settings.py.
  * Remove bogus docs/._index.rst ahead of building documentation.
  * Streamline packaging code with wrap-and-sort.
  * Switch to dh_python2.
  * Update database settings for unit tests to comply with Django 1.3.
  * Clean up documentation after build.
  * Bumped Standards-Version to 3.9.2 (no change necessary).
  * Use dh_sphinxdoc.
  * Change build path for documentation.
  * Ship documentation in separate package.
  * Use override_dh_auto_build to build documentation.
  * Update copyright year in d/copyright.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Mon, 03 Oct 2011 07:20:07 +0200

django-auth-ldap (1.0.7-1) experimental; urgency=low

  * New upstream release.
  * Bumped version of Debian policy compliance to 3.9.1 (nothing to do).
  * Include documentation. (Closes: #600634)

 -- Fladischer Michael <FladischerMichael@fladi.at>  Thu, 30 Dec 2010 23:19:00 +0100

django-auth-ldap (1.0.5-1) unstable; urgency=low

  * New upstream release.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Tue, 22 Jun 2010 10:59:16 +0200

django-auth-ldap (1.0.4-1) UNRELEASED; urgency=low

  * New upstream release.
  * Add Vcs-* fields.
  * Set Debian Python Modules Packaging Team as Uploaders.
  * Use DEP5 for copyright file.

 -- Fladischer Michael <FladischerMichael@fladi.at>  Mon, 07 Jun 2010 08:21:29 +0200

django-auth-ldap (1.0.3-1) unstable; urgency=low

  * Initial release (Closes: #575817)

 -- Fladischer Michael <FladischerMichael@fladi.at>  Fri, 02 Apr 2010 08:48:43 +0200
